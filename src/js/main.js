$(document).ready(function () {

  //Load page at bottom of document
  window.scrollTo(0, document.body.clientHeight);

  //Hide navbar when scrolling up & show when scrolling down
  $(function(){
   var lastScrollTop = 0, delta = 5;
   $(window).scroll(function(){
  	 var nowScrollTop = $(this).scrollTop();
  	 if(Math.abs(lastScrollTop - nowScrollTop) >= delta){
  	 	if (nowScrollTop > lastScrollTop){
  	 		// Scrolldown
        $('.navbar').fadeIn();
  	 	} else {
  	 		// Scrollup
        $('.navbar').fadeOut();
  		}
  	 lastScrollTop = nowScrollTop;
  	 }
   });
  });


});
